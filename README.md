## Simple node.js API for User authentication

1. Run **npm install** to update dependencies
2. Run **npm start** to start the API server
3. *Note: test coverage is currently not implemented*
4. *Note: email feature is currently not implemented*
---

## Issues while implementing

1. Issues with debugging, not being sure why some things are breaking ex. why my Authentication isn't working when I have received my token successfully.
2. Having started the application as a whole, without breaking it down into simple features.
3. Couldn't figure out the issues with the test setup (after some investigation found out it might be a compiler issue and having a custom one locally might work).
4. 'Copy/paste' errors.
5. Using node.js was maybe not a great choice because I don't have that much experience with it. If I had used Rails, I would have been able to implement the mailer feature as well, since I am familiar with Action::Mailer.
---
